**The 7th Continent : Sounds and light project** is a sensory work whose purpose is to recreate the atmosphere of the Great Pacific Garbage Patch. 

By connecting sounds and light we aim to immerse  the spectators perceptions, drowning them in three sensory perspectives : the sound of plastic into water, the sound of animal distress, and the visual deterioration of corals. 

We will combine creaky sounds, clashes and the  increasing intensity of animal distress sounds to demonstrate the density of the plastic clashing.

The spectator will perceive through the increase of the recurrent sounds, the evolution of the Great Pacific Garbage Patch’s density. 

To recreate sounds of animal distress, we will mix the sound recording that french freediver Guillaume Nery made and a music digital creation. The volume level will have to be intensify as the plastic sounds recurrence increases. 

For the visual effect, the accelerated disintegration of corals will be projected on all the walls of the room. 

All of this, during 3 minutes, will be accompanied by a luminous modulation, from pink, ocher, grey and finally black, figuring the colorful evolution of coral’s destruction. This final black will represent ocean’s annihilation. 